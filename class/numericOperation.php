<?php

class numericOperation {
    /**
     * @var array $result Массив для результатов выполнения операций "+", "-", "*"
     */
    public $result = array (
        'arg1' => '',
        'arg2' => '',
        'result' => '',
        'mark' => '' // знак операции
    );
    /**
     * @var array $resultMany массив для "+-" операции
     */
    public $resultMany = array (
        array(), // массив аргументов
        array(), // массив арифметических знаков
        'Result' => 0 // результат вычислений. И временная сумма записывается, и конечный результат
    );
    /**
     * @var array $temp массив для временного хранения значений
     */
    private $temp = array (
        'arg1' => '',
        'arg2' => ''
    );
    /**
     * @var array $splitArg1 массив, в который разбиваем первый аргумент на цифры
     */
    private $splitArg1 = array();
    /**
     * @var array $splitArg2 массив, в который разбиваем второй аргумент на цифры
     */
    private $splitArg2 = array();
    private $n1;
    private $n2;
    /**
     * @var $splitArg2 максимальное число для просчетов, верхняя граница диапазона для RAND
     */
    private $maxNumber;
    /**
     * @var $thrueDigit. y - значит учитываем переход через разряд, n - без перехода через разряд
     */
    private $thrueDigit;
    /**
     * @var $quantityNumber количество чисел для варианта "+-"
     */
    public $quantityNumber;
    private $variable;
    private $plusOrMinus;

    public function Sub($MaxNumber, $thrueDigit) {
        $this->maxNumber = $MaxNumber;
        $this->thrueDigit = $thrueDigit;
        if ($this->thrueDigit == 'n') { // если вычитание без переполнения разряда
            $this->temp['arg1'] = mt_rand(0, $this->maxNumber); // делаем первое число
            if ($this->temp['arg1'] == 0) { // если первое число 0, то второе тоже 0
                $this->result['arg1'] = $this->temp['arg1'];
                $this->result['arg2'] = 0;
                $this->result['result'] = $this->result['arg1'] - $this->result['arg2'];
                $this->result['mark'] = "-";
                return $this->result;
            } elseif ($this->temp['arg1'] == $this->maxNumber) { // если первое число максимальное (10, 100) -
                    //то второе считаем любое, т.к. типа нет переполнения у нас
                $this->result['arg1'] = $this->temp['arg1'];
                $this->result['arg2'] = mt_rand(0, $this->result['arg1']);
                $this->result['result'] = $this->result['arg1'] - $this->result['arg2'];
                $this->result['mark'] = "-";
                return $this->result;
            } else {
                $splitArg1 = preg_split('//', $this->temp['arg1'], -1, PREG_SPLIT_NO_EMPTY); // массив из цифр
                    //первого числа
                $n1 = count($splitArg1) - 1; // n1 - указатель в массиве $splitArg1 на единицы числа
                $this->temp['arg2'] = mt_rand(0, $this->temp['arg1']); // второе число, не больше
                    //первого уже полученного
                $splitArg2 = preg_split('//', $this->temp['arg2'], -1, PREG_SPLIT_NO_EMPTY);// массив из цифр
                    //второго числа
                $n2 = count($splitArg2) - 1; // n2 - указатель в массиве $splitArg2 на единицы числа
                $splitArg2[$n2] = mt_rand(0, $splitArg1[$n1]); // единицы второго числа, чтобы не было
                    //переполнения через разряд
                $this->result['arg1'] = implode($splitArg1);
                $this->result['arg2'] = implode($splitArg2);
                $this->result['result'] = $this->result['arg1'] - $this->result['arg2'];
                $this->result['mark'] = "-"; // запоминаем знак операции
                return $this->result;
            }
        } elseif ($this->thrueDigit == 'y') { // операция вычитания и если нужно переполнение
            if ($this->maxNumber <= 10) {
                print "При max < или = 10 нельзя сформировать пример с переполнением разряда";
            } else {
                $this->temp['arg1'] = mt_rand(11, $this->maxNumber);
                if ($this->temp['arg1'] == $this->maxNumber) {
                    $this->result['arg1'] = $this->temp['arg1'];
                    $this->result['arg2'] = mt_rand(0, $this->temp['arg1'] - 1);
                    $this->result['result'] = $this->result['arg1'] - $this->result['arg2'];
                    $this->result['mark'] = "-";
                    return $this->result;
                } else {
                    $splitArg1 = preg_split('//', $this->temp['arg1'], -1, PREG_SPLIT_NO_EMPTY); // массив из цифр
                        //первого числа
                    $this->temp['arg2'] = mt_rand(0, $this->temp['arg1']);
                    $splitArg2 = preg_split('//', $this->temp['arg2'], -1, PREG_SPLIT_NO_EMPTY); // массив из цифр
                        //второго числа
                    $n1 = count($splitArg1) - 1; // n1 - указатель в массиве $splitArg1 на единицы первого числа
                    $n2 = count($splitArg2) - 1; // n2 - указатель в массиве $splitArg2 на единицы второго числа
                        if ($splitArg1[$n1] == 9) {
                            $splitArg1[$n1] = mt_rand(1, 8); // единицы первого числа от 1 до 8.
                        } //Если будет 9 - то не будет переполнения, отсекаем девятку
                    $splitArg2[$n2] = mt_rand($splitArg1[$n1] + 1, 9); // единицы второго числа, чтобы было
                        //переполнение через разряд
                    $n1 = count($splitArg1) - 2;
                    $n2 = count($splitArg2) - 2;
                        if ($n2 >= 0) {
                    if ($splitArg2[$n2] != NULL) {
                            $splitArg2[$n2] = mt_rand(0, $splitArg1[$n1] - 1);
                            if ($splitArg2[$n2] == 0) {
                                $splitArg2[$n2] = NULL;
                            }
                    }
                           // если десятки
                       //первого и второго аргумента одинаковые, то избегаем ошибки с отрицательным результатом
                        //(единицы второго больше чем первого аргумента, можем сделать меньше нуля)
                        }

                    $this->result['arg1'] = implode($splitArg1);
                    $this->result['arg2'] = implode($splitArg2);
                    $this->result['result'] = $this->result['arg1'] - $this->result['arg2'];
                    $this->result['mark'] = "-";
                    return $this->result;
                }
            }
        } else print "Что с переходом через разряд? Ни 'y' ни 'n'";
        return 1;
    }
    public function Add($MaxNumber, $thrueDigit) {
        $this->maxNumber = $MaxNumber;
        $this->thrueDigit = $thrueDigit;
        if ($this->thrueDigit == 'n') {
            $this->temp['arg1'] = mt_rand(0, $this->maxNumber);
            if ($this->temp['arg1'] == $this->maxNumber) {
                $this->result['arg1'] = $this->maxNumber;
                $this->result['arg2'] = 0;
                $this->result['result'] = $this->result['arg1'] + $this->result['arg2'];
                $this->result['mark'] = "+";
                return $this->result;
            }
            elseif ($this->temp['arg1'] == 0) {
                $this->result['arg1'] = $this->temp['arg1'];
                $this->result['arg2'] = mt_rand(0, $this->maxNumber);
                $this->result['result'] = $this->result['arg1'] + $this->result['arg2'];
                $this->result['mark'] = "+";
                return $this->result;
            }
            else {
                $splitArg1 = preg_split('//', $this->temp['arg1'], -1, PREG_SPLIT_NO_EMPTY); // массив из цифр
                    //первого числа
                $n1 = count($splitArg1)-1; // n1 - указатель в массиве $splitArg1 на единицы числа
                $this->result['arg1'] = implode ($splitArg1);
                $this->temp['arg2'] = mt_rand(0, $this->maxNumber - $this->temp['arg1']); // чтобы без переполнения
                    //через максимальное число
                $splitArg2 = preg_split('//', $this->temp['arg2'], -1, PREG_SPLIT_NO_EMPTY);
                $n2 = count($splitArg2)-1; // n2 - указатель в массиве $splitArg2 на единицы числа
                $splitArg2[$n2] = mt_rand(0, 9 - $splitArg1[$n1]); // единицы второго числа, чтобы не было
                    //переполнения через разряд
                $this->result['arg2'] = implode ($splitArg2); // объединяем (implode) массив из цифр второго
                    //аргумента в строку, после делаем её числом (strval)
                $this->result['result'] = $this->result['arg1'] + $this->result['arg2'];
                $this->result['mark'] = "+";
                return $this->result;
            }
        }
        elseif ($this->thrueDigit == 'y') {
            if ($this->maxNumber == 10) $this->maxNumber = $this->maxNumber-1; // исключаем 10 в рендоме, т.к.
                //возможно появление ошибки в алгоритме
            if ($this->maxNumber >= 20) $this->maxNumber = $this->maxNumber-11; // первый аргумент меньше
                //$this->maxNumber на десять, чтобы можно быдо сделать пример с переполнением разряда и не выйти
                //за пределы $this->maxNumber.
            $this->temp['arg1'] = mt_rand(0, $this->maxNumber);
            $splitArg1 = preg_split('//', $this->temp['arg1'], -1, PREG_SPLIT_NO_EMPTY); // массив из цифр первого числа
            $n1 = count($splitArg1)-1; // n1 - указатель в массиве $splitArg1 на единицы числа
            $splitArg1[$n1]= rand(2, 9); // делаем единицы первого числа так, чтобы в сумме со вторым аргументов
                //у нас обязательно было переполнение разряда
            $this->result['arg1'] = implode ($splitArg1);
            $this->temp['arg2'] = mt_rand(0, $this->maxNumber - $this->result['arg1']);
            $splitArg2 = preg_split('//', $this->temp['arg2'], -1, PREG_SPLIT_NO_EMPTY);
            $n2 = count($splitArg2)-1; // n2 - указатель в массиве $splitArg2 на единицы числа
            switch ($splitArg1[$n1]) {
                case 2:
                    $splitArg2[$n2] = 9;
                    break;
                case 3:
                    $splitArg2[$n2] = mt_rand(8, 9);
                    break;
                case 4:
                    $splitArg2[$n2] = mt_rand(7, 9);
                    break;
                case 5:
                    $splitArg2[$n2] = mt_rand(6, 9);
                    break;
                case 6:
                    $splitArg2[$n2] = mt_rand(5, 9);
                    break;
                case 7:
                    $splitArg2[$n2] = mt_rand(4, 9);
                    break;
                case 8:
                    $splitArg2[$n2] = mt_rand(3, 9);
                    break;
                case 9:
                    $splitArg2[$n2] = mt_rand(2, 9);
                    break;
            }
            $this->result['arg2'] = strval(implode ($splitArg2));
            $this->result['result'] = $this->result['arg1'] + $this->result['arg2'];
            $this->result['mark'] = "+";
            return $this->result;
        }
        else {
            print "Что с переходом через разряд? Ни 'y' ни 'n'";
            return 1;
        }
    }
    public function variablePlusMinus($MaxNumber, $quantityNumber) {
        $this->maxNumber = $MaxNumber;
        $this->quantityNumber = $quantityNumber;

        $this->resultMany[0][0] = mt_rand(0, $this->maxNumber); // генерируем первое число выражения
        $this->resultMany['result'] = $this->resultMany[0][0]; // промежуточная сумма из одного числа выражения
        $plusOrMinus = mt_rand(0, 1);
        if ($plusOrMinus == 1) $this->resultMany["1"]["0"] = "+"; // запоминаем знак первой операции, плюс или минус.
        else $this->resultMany["1"]["0"] = "-";
        for ($i = 1; $i <= $this->quantityNumber - 1; $i++) {
            if ($plusOrMinus == 1) { // если плюс...
                $this->resultMany[0][$i] = mt_rand(0, $this->maxNumber - $this->resultMany['result']); // делаем число,
                    //чтобы не было переполнения от суммирования с промежуточной суммой на этом этапе
                $this->resultMany['result'] += $this->resultMany[0][$i]; // увелиливаем промежуточную сумму
            } else { // если минус...
                $this->resultMany[0][$i] = mt_rand(0, $this->resultMany['result']);
                $this->resultMany['result'] -= $this->resultMany[0][$i];
            }
                if ($i != $this->quantityNumber-1) {
                $plusOrMinus = mt_rand(0, 1);
                if ($plusOrMinus == 1) $this->resultMany["1"]["$i"] = "+";
                else $this->resultMany["1"]["$i"] = "-";
                }
        }
        for ($i = 6; $i > $this->quantityNumber; $i--) {
            $this->resultMany["1"]["" . $i-2 . ""] = NULL;
            $this->resultMany["0"]["" . $i-1 . ""] = NULL;
        }
        return $this->resultMany;
    }
    public function Multiplication($variable) {
        $this->variable = $variable;
        switch (true) {
            case $this->variable >=2 and $this->variable <= 10:
                $this->result['arg1'] = $this->variable;
                $this->result['arg2'] = mt_rand(0, 10);
                $this->result['mark'] = '*';
                $this->result['result'] = $this->result['arg1'] * $this->result['arg2'];
                return $this->result;
                break;
            case $this->variable == "variable":
                $this->result['arg1'] = mt_rand(2, 9);
                $this->result['arg2'] = mt_rand(2, 9);
                $this->result['mark'] = '*';
                $this->result['result'] = $this->result['arg1'] * $this->result['arg2'];
                return $this->result;
                break;
            default:
                print "Некорректно введен аргумент метода. Либо число от 2 по 10, либо значение 'variable'";
                break;
        }
    }
}
?>