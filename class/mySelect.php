<?php
class mySelect {
    public $data_locale;
    public function __construct () {
        $check2 = mysql_query("SELECT locale FROM locale_table");
        $checkData2 = mysql_fetch_array($check2);
        switch ($checkData2['locale']) {
            case 'russian':
                $var_locale = 'locale_russian';
                break;
            case 'english':
                $var_locale = 'locale_english';
                break;
            case 'NULL':
                $var_locale = 'NULL';
                break;
            default:
                $var_locale = 'NULL';
        }
        $locale = mysql_query("SELECT * FROM $var_locale");
        $this->data_locale = mysql_fetch_array($locale);
    }
    public function ifAdd() {
        printf('
                <form action = "condition.php" method = "POST" name = "form" target = "_self">
                <input name = "typeOfOperation" type = "hidden" value = "add">
                <fieldset>
                    <legend>%s</legend>
                    <p><select name="typeOfOperation" size="1">
                            <option selected value="add">%s</option>
                    </select></p>
                </fieldset>
                    <fieldset>
                    <legend>%s</legend>
                    <p><select name="maxNumber" size="1">
                            <option selected value="null"></option>
                            <option value="10">0 ... 10</option>
                            <option value="20">0 ... 20</option>
                            <option value="100">0 ... 100</option>
                    </select></p>
                    </fieldset>
                        <fieldset>
                        <legend>%s</legend>
                        <p><select name="thrueDigit" size="1">
                                <option selected value="null"></option>
                                <option value="y">%s</option>
                                <option value="n">%s</option>
                        </select></p>
                        </fieldset>
                            <fieldset>
                            <legend>%s</legend>
                            <p><select name="numberOfOperation" size="1">
                                    <option selected value="null"></option>
                                    <option value="1">%s</option>
                                    <option value="5">%s</option>
                                    <option value="10">%s</option>
                                    <option value="20">%s</option>
                            </select></p>
                            </fieldset>
                        <p><input type="submit" value="%s" class="itemParametersEnd"></p>
                    </form>
                ', $this->data_locale['mySelectLegend'], $this->data_locale['mySelectAdd'],
                $this->data_locale['mySelectIfAddLegend_2'], $this->data_locale['mySelectIfAddLegend_3'],
                $this->data_locale['mySelectIfAddThrueDigitY'], $this->data_locale['mySelectIfAddThrueDigitN'],
                $this->data_locale['mySelectNumberOfTest'], $this->data_locale['mySelectNumOfTest_1'],
                $this->data_locale['mySelectNumOfTest_5'], $this->data_locale['mySelectNumOfTest_10'],
                $this->data_locale['mySelectNumOfTest_20'], $this->data_locale['mySelectSubmit']);
    }
    public function ifSub() {
        printf('
            <form action = "condition.php" method = "POST" name = "form" target = "_self">
            <input name = "typeOfOperation" type = "hidden" value = "sub">
            <fieldset>
                <legend>%s</legend>
                <p><select name="typeOfOperation" size="1">
                        <option selected value="sub">%s</option>
                </select></p>
            </fieldset>
                <fieldset>
                <legend>%s</legend>
                <p><select name="maxNumber" size="1">
                        <option selected value="null"></option>
                        <option value="10">0 ... 10</option>
                        <option value="20">0 ... 20</option>
                        <option value="100">0 ... 100</option>
                </select></p>
                </fieldset>
                    <fieldset>
                    <legend>%s</legend>
                    <p><select name="thrueDigit" size="1">
                            <option selected value="null"></option>
                            <option value="y">%s</option>
                            <option value="n">%s</option>
                    </select></p>
                    </fieldset>
                            <fieldset>
                            <legend>%s</legend>
                            <p><select name="numberOfOperation" size="1">
                                    <option selected value="null"></option>
                                    <option value="1">%s</option>
                                    <option value="5">%s</option>
                                    <option value="10">%s</option>
                                    <option value="20">%s</option>
                            </select></p>
                            </fieldset>

                    <p><input type="submit" value="%s"  class="itemParametersEnd"></p>
            </form>
            ', $this->data_locale['mySelectLegend'], $this->data_locale['mySelectSub'],
            $this->data_locale['mySelectIfSubLegend_2'], $this->data_locale['mySelectIfSubLegend_3'],
            $this->data_locale['mySelectIfSubThrueDigitY'], $this->data_locale['mySelectIfSubThrueDigitN'],
            $this->data_locale['mySelectNumberOfTest'], $this->data_locale['mySelectNumOfTest_1'],
            $this->data_locale['mySelectNumOfTest_5'], $this->data_locale['mySelectNumOfTest_10'],
            $this->data_locale['mySelectNumOfTest_20'], $this->data_locale['mySelectSubmit']);
    }
    public function ifAddOrSub() {
        include "block/locale.php";
        printf('
            <form action = "condition.php" method = "POST" name = "form" target = "_self">
            <input name = "typeOfOperation" type = "hidden" value = "addOrSub">
            <fieldset>
                <legend>%s</legend>
                <p><select name="typeOfOperation" size="1">
                        <option selected value="addOrSub">%s</option>
                </select></p>
            </fieldset>
                <fieldset>
                <legend>%s</legend>
                <p><select name="maxNumber" size="1">
                        <option selected value="null"></option>
                        <option value="10">0 ... 10</option>
                        <option value="20">0 ... 20</option>
                        <option value="100">0 ... 100</option>
                </select></p>
                </fieldset>
                    <fieldset>
                    <legend>%s</legend>
                    <p><select name="quantityNumber" size="1">
                            <option selected value="null"></option>
                            <option value="2">%s</option>
                            <option value="3">%s</option>
                            <option value="4">%s</option>
                            <option value="5">%s</option>
                            <option value="6">%s</option>
                    </select></p>
                    </fieldset>
                            <fieldset>
                            <legend>%s</legend>
                            <p><select name="numberOfOperation" size="1">
                                    <option selected value="null"></option>
                                    <option value="1">%s</option>
                                    <option value="5">%s</option>
                                    <option value="10">%s</option>
                                    <option value="20">%s</option>
                            </select></p>
                            </fieldset>
                    <p><input type="submit" value="%s"  class="itemParametersEnd"></p>
            </form>
            ', $this->data_locale['mySelectLegend'], $this->data_locale['mySelectAddOrSub'],
            $this->data_locale['mySelectAddOrSubLegend_1'], $this->data_locale['mySelectAddOrSubLegend_2'],
            $this->data_locale['mySelectAddOrSubQuantNumber_2'], $this->data_locale['mySelectAddOrSubQuantNumber_3'],
            $this->data_locale['mySelectAddOrSubQuantNumber_4'], $this->data_locale['mySelectAddOrSubQuantNumber_5'],
            $this->data_locale['mySelectAddOrSubQuantNumber_6'], $this->data_locale['mySelectNumberOfTest'],
            $this->data_locale['mySelectNumOfTest_1'], $this->data_locale['mySelectNumOfTest_5'],
            $this->data_locale['mySelectNumOfTest_10'], $this->data_locale['mySelectNumOfTest_20'],
            $this->data_locale['mySelectSubmit']);
    }
    public function ifMultiplication() {
        include "block/locale.php";
        printf('
            <form action = "condition.php" method = "POST" name = "form" target = "_self">
            <input name = "typeOfOperation" type = "hidden" value = "Multiplication">
            <fieldset>
                <legend>%s</legend>
                <p><select name="typeOfOperation" size="1">
                        <option selected value="Multiplication">%s</option>
                </select></p>
            </fieldset>
                <fieldset>
                <legend>%s</legend>
                <p><select name="variable" size="1">
                        <option selected value="null"></option>
                        <option value="2">%s 2</option>
                        <option value="3">%s 3</option>
                        <option value="4">%s 4</option>
                        <option value="5">%s 5</option>
                        <option value="6">%s 6</option>
                        <option value="7">%s 7</option>
                        <option value="8">%s 8</option>
                        <option value="9">%s 9</option>
                        <option value="10">%s 10</option>
                        <option value="variable">%s</option>
                </select></p>
            </fieldset>
                            <fieldset>
                            <legend>%s</legend>
                            <p><select name="numberOfOperation" size="1">
                                <option selected value="null"></option>
                                    <option value="1">%s</option>
                                    <option value="5">%s</option>
                                    <option value="10">%s</option>
                                    <option value="20">%s</option>
                            </select></p>
                            </fieldset>
            <p><input type="submit" value="%s"  class="itemParametersEnd"></p>
            </form>
            ', $this->data_locale['mySelectLegend'], $this->data_locale['mySelectMultiplication'],
            $this->data_locale['mySelectMultLegend_1'],$this->data_locale['mySelectMultFrom'],
            $this->data_locale['mySelectMultFrom'], $this->data_locale['mySelectMultFrom'],
            $this->data_locale['mySelectMultFrom'], $this->data_locale['mySelectMultFrom'],
            $this->data_locale['mySelectMultFrom'], $this->data_locale['mySelectMultFrom'],
            $this->data_locale['mySelectMultFrom'], $this->data_locale['mySelectMultFrom'],
            $this->data_locale['mySelectMultVari'], $this->data_locale['mySelectNumberOfTest'],
            $this->data_locale['mySelectNumOfTest_1'], $this->data_locale['mySelectNumOfTest_5'],
            $this->data_locale['mySelectNumOfTest_10'], $this->data_locale['mySelectNumOfTest_20'],
            $this->data_locale['mySelectSubmit']);
    }
    public function ifEquation() {
        include "block/locale.php";
        printf('
            <form action = "condition.php" method = "POST" name = "form" target = "_self">
            <input name = "typeOfOperation" type = "hidden" value = "equation">
            <input name="quantityNumber" type="hidden" value="2">
            <fieldset>
                <legend>%s</legend>
                <p><select name="typeOfOperation" size="1">
                        <option selected value="equation">%s</option>
                </select></p>
            </fieldset>
                <fieldset>
                <legend>%s</legend>
                <p><select name="maxNumber" size="1">
                        <option selected value="null"></option>
                        <option value="20">0 ... 20</option>
                        <option value="100">0 ... 100</option>
                </select></p>
                </fieldset>
                            <fieldset>
                            <legend>%s</legend>
                            <p><select name="numberOfOperation" size="1">
                                    <option selected value="null"></option>
                                    <option value="1">%s</option>
                                    <option value="5">%s</option>
                                    <option value="10">%s</option>
                                    <option value="20">%s</option>
                            </select></p>
                            </fieldset>
                    <p><input type="submit" value="%s"  class="itemParametersEnd"></p>
            </form>
            ', $this->data_locale['mySelectLegend'], $this->data_locale['mySelectEquation'],
            $this->data_locale['mySelectEquatLegend_1'], $this->data_locale['mySelectNumberOfTest'],
            $this->data_locale['mySelectNumOfTest_1'], $this->data_locale['mySelectNumOfTest_5'],
            $this->data_locale['mySelectNumOfTest_10'], $this->data_locale['mySelectNumOfTest_20'],
            $this->data_locale['mySelectSubmit']);
    }
    public function ifDefault() {
        include "block/locale.php";
        printf('
            <form action = "index2.php" method = "POST" name = "form" target = "_self">
            <fieldset>
                <legend>%s</legend>
                <p><select name="typeOfOperation" size="1" onchange="this.form.submit()">
                        <option selected value="null"></option>
                        <option value="add">%s</option>
                        <option value="sub">%s</option>
                        <option value="addOrSub">%s</option>
                        <option value="Multiplication">%s</option>
                        <option value="equation">%s</option>
                </select></p>
            </fieldset>
            </form>
            ', $this->data_locale['mySelectLegend'], $this->data_locale['mySelectAdd'],
            $this->data_locale['mySelectSub'], $this->data_locale['mySelectAddOrSub'],
            $this->data_locale['mySelectMultiplication'], $this->data_locale['mySelectEquation']);
    }
}
?>