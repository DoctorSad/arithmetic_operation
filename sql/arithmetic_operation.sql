-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июл 03 2015 г., 16:33
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `arithmetic_operation`
--

-- --------------------------------------------------------

--
-- Структура таблицы `locale_english`
--

CREATE TABLE IF NOT EXISTS `locale_english` (
  `indexMainTitle` varchar(100) NOT NULL,
  `indexTitle` varchar(100) NOT NULL,
  `mySelectAdd` varchar(100) NOT NULL,
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `mySelectSub` varchar(100) NOT NULL,
  `mySelectAddOrSub` varchar(100) NOT NULL,
  `mySelectMultiplication` varchar(100) NOT NULL,
  `mySelectEquation` varchar(100) NOT NULL,
  `mySelectSubmit` varchar(100) NOT NULL,
  `mySelectLegend` varchar(100) NOT NULL,
  `mySelectIfAddLegend_2` varchar(100) NOT NULL,
  `mySelectIfAddLegend_3` varchar(100) NOT NULL,
  `mySelectIfAddThrueDigitY` varchar(100) NOT NULL,
  `mySelectIfAddThrueDigitN` varchar(100) NOT NULL,
  `mySelectNumberOfTest` varchar(100) NOT NULL,
  `mySelectNumOfTest_1` varchar(100) NOT NULL,
  `mySelectNumOfTest_5` varchar(100) NOT NULL,
  `mySelectNumOfTest_10` varchar(100) NOT NULL,
  `mySelectNumOfTest_20` varchar(100) NOT NULL,
  `mySelectIfSubLegend_2` varchar(100) NOT NULL,
  `mySelectIfSubLegend_3` varchar(100) NOT NULL,
  `mySelectIfSubThrueDigitY` varchar(100) NOT NULL,
  `mySelectIfSubThrueDigitN` varchar(100) NOT NULL,
  `mySelectAddOrSubLegend_1` varchar(100) NOT NULL,
  `mySelectAddOrSubLegend_2` varchar(100) NOT NULL,
  `mySelectAddOrSubQuantNumber_2` varchar(100) NOT NULL,
  `mySelectAddOrSubQuantNumber_3` varchar(100) NOT NULL,
  `mySelectAddOrSubQuantNumber_4` varchar(100) NOT NULL,
  `mySelectAddOrSubQuantNumber_5` varchar(100) NOT NULL,
  `mySelectAddOrSubQuantNumber_6` varchar(100) NOT NULL,
  `mySelectMultLegend_1` varchar(100) NOT NULL,
  `mySelectMultFrom` varchar(100) NOT NULL,
  `mySelectMultVari` varchar(100) NOT NULL,
  `handlerSubmit` varchar(100) NOT NULL,
  `wrongOrThrueSubmit` varchar(100) NOT NULL,
  `mySelectEquatLegend_1` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `locale_english`
--

INSERT INTO `locale_english` (`indexMainTitle`, `indexTitle`, `mySelectAdd`, `id`, `mySelectSub`, `mySelectAddOrSub`, `mySelectMultiplication`, `mySelectEquation`, `mySelectSubmit`, `mySelectLegend`, `mySelectIfAddLegend_2`, `mySelectIfAddLegend_3`, `mySelectIfAddThrueDigitY`, `mySelectIfAddThrueDigitN`, `mySelectNumberOfTest`, `mySelectNumOfTest_1`, `mySelectNumOfTest_5`, `mySelectNumOfTest_10`, `mySelectNumOfTest_20`, `mySelectIfSubLegend_2`, `mySelectIfSubLegend_3`, `mySelectIfSubThrueDigitY`, `mySelectIfSubThrueDigitN`, `mySelectAddOrSubLegend_1`, `mySelectAddOrSubLegend_2`, `mySelectAddOrSubQuantNumber_2`, `mySelectAddOrSubQuantNumber_3`, `mySelectAddOrSubQuantNumber_4`, `mySelectAddOrSubQuantNumber_5`, `mySelectAddOrSubQuantNumber_6`, `mySelectMultLegend_1`, `mySelectMultFrom`, `mySelectMultVari`, `handlerSubmit`, `wrongOrThrueSubmit`, `mySelectEquatLegend_1`) VALUES
('Arithmetic operation', 'Home Page', 'Addition', 1, 'Subtraction', 'Addition and (or) Subtraction (randomize)', 'Multiplication', 'Solving of equations', 'Send', 'Select the arithmetic operation:', 'Specify the range in which we will summarize numbers:', 'Addition with discharge overflow or not?', 'With overflow', 'Without overflow', 'Select the number of examples:', 'One example', 'Five examples', 'Ten examples', 'Twenty examples', 'Specify the range in which we will substract numbers:', 'Substract with discharge overflow or not?', 'With overflow', 'Without overflow', 'Specify the range in which we will substract/summarize numbers:', 'Specify the range of numbers to sum or subtraction:', '2 numbers', '3 numbers', '4 numbers', '5 numbers', '6 numbers', 'Select the options for multiplication:', 'By', 'Random selection of combination', 'Send', 'Repeat?', 'Select a range of numbers for the compilation of equation');

-- --------------------------------------------------------

--
-- Структура таблицы `locale_russian`
--

CREATE TABLE IF NOT EXISTS `locale_russian` (
  `indexMainTitle` varchar(100) NOT NULL,
  `indexTitle` varchar(100) NOT NULL,
  `mySelectAdd` varchar(100) NOT NULL,
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `mySelectSub` varchar(100) NOT NULL,
  `mySelectAddOrSub` varchar(100) NOT NULL,
  `mySelectMultiplication` varchar(100) NOT NULL,
  `mySelectEquation` varchar(100) NOT NULL,
  `mySelectSubmit` varchar(100) NOT NULL,
  `mySelectLegend` varchar(100) NOT NULL,
  `mySelectIfAddLegend_2` varchar(100) NOT NULL,
  `mySelectIfAddLegend_3` varchar(100) NOT NULL,
  `mySelectIfAddThrueDigitY` varchar(100) NOT NULL,
  `mySelectIfAddThrueDigitN` varchar(100) NOT NULL,
  `mySelectNumberOfTest` varchar(100) NOT NULL,
  `mySelectNumOfTest_1` varchar(100) NOT NULL,
  `mySelectNumOfTest_5` varchar(100) NOT NULL,
  `mySelectNumOfTest_10` varchar(100) NOT NULL,
  `mySelectNumOfTest_20` varchar(100) NOT NULL,
  `mySelectIfSubLegend_2` varchar(100) NOT NULL,
  `mySelectIfSubLegend_3` varchar(100) NOT NULL,
  `mySelectIfSubThrueDigitY` varchar(100) NOT NULL,
  `mySelectIfSubThrueDigitN` varchar(100) NOT NULL,
  `mySelectAddOrSubLegend_1` varchar(100) NOT NULL,
  `mySelectAddOrSubLegend_2` varchar(100) NOT NULL,
  `mySelectAddOrSubQuantNumber_2` varchar(100) NOT NULL,
  `mySelectAddOrSubQuantNumber_3` varchar(100) NOT NULL,
  `mySelectAddOrSubQuantNumber_4` varchar(100) NOT NULL,
  `mySelectAddOrSubQuantNumber_5` varchar(100) NOT NULL,
  `mySelectAddOrSubQuantNumber_6` varchar(100) NOT NULL,
  `mySelectMultLegend_1` varchar(100) NOT NULL,
  `mySelectMultFrom` varchar(100) NOT NULL,
  `mySelectMultVari` varchar(100) NOT NULL,
  `handlerSubmit` varchar(100) NOT NULL,
  `wrongOrThrueSubmit` varchar(100) NOT NULL,
  `mySelectEquatLegend_1` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `locale_russian`
--

INSERT INTO `locale_russian` (`indexMainTitle`, `indexTitle`, `mySelectAdd`, `id`, `mySelectSub`, `mySelectAddOrSub`, `mySelectMultiplication`, `mySelectEquation`, `mySelectSubmit`, `mySelectLegend`, `mySelectIfAddLegend_2`, `mySelectIfAddLegend_3`, `mySelectIfAddThrueDigitY`, `mySelectIfAddThrueDigitN`, `mySelectNumberOfTest`, `mySelectNumOfTest_1`, `mySelectNumOfTest_5`, `mySelectNumOfTest_10`, `mySelectNumOfTest_20`, `mySelectIfSubLegend_2`, `mySelectIfSubLegend_3`, `mySelectIfSubThrueDigitY`, `mySelectIfSubThrueDigitN`, `mySelectAddOrSubLegend_1`, `mySelectAddOrSubLegend_2`, `mySelectAddOrSubQuantNumber_2`, `mySelectAddOrSubQuantNumber_3`, `mySelectAddOrSubQuantNumber_4`, `mySelectAddOrSubQuantNumber_5`, `mySelectAddOrSubQuantNumber_6`, `mySelectMultLegend_1`, `mySelectMultFrom`, `mySelectMultVari`, `handlerSubmit`, `wrongOrThrueSubmit`, `mySelectEquatLegend_1`) VALUES
('Арифметические операции', 'На главную страницу', 'Сложение', 1, 'Вычитание', 'Сложение и(или) вычитание (рэндомно)', 'Умножение', 'Решение уравнений', 'Отправить', 'Выберите арифметическую операцию:', 'Укажите диапазон в котором будем складывать числа:', 'Сложение с переполнением разряда или нет?', 'С переполнением', 'Без переполнения', 'Выберите количество примеров:', 'Один пример', 'Пять примеров', 'Десять примеров', 'Двадцать примеров', 'Укажите диапазон в котором будем вычитать числа:', 'Вычитание с переходом через десяток?', 'С переходом', 'Без перехода', 'Укажите диапазон в котором будем складывать/вычитать числа:', 'Укажите количество чисел для суммирования или вычитания:', '2 числа', '3 числа', '4 числа', '5 числа', '6 числа', 'Выберите вариант умножения:', 'На', 'Случайный выбор комбинации', 'Отправить', 'Повторить?', 'Выберите диапазон чисел для составления уравнений:');

-- --------------------------------------------------------

--
-- Структура таблицы `locale_table`
--

CREATE TABLE IF NOT EXISTS `locale_table` (
  `locale` varchar(50) NOT NULL,
  `id` int(5) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Дамп данных таблицы `locale_table`
--

INSERT INTO `locale_table` (`locale`, `id`) VALUES
('english', 58);

-- --------------------------------------------------------

--
-- Структура таблицы `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `arg1` varchar(5) NOT NULL,
  `mark1` text NOT NULL,
  `arg2` varchar(5) NOT NULL,
  `mark2` varchar(5) NOT NULL,
  `arg3` varchar(5) NOT NULL,
  `mark3` varchar(5) NOT NULL,
  `arg4` varchar(5) NOT NULL,
  `mark4` varchar(5) NOT NULL,
  `arg5` varchar(5) NOT NULL,
  `mark5` varchar(5) NOT NULL,
  `arg6` varchar(5) NOT NULL,
  `result` varchar(5) NOT NULL,
  `resultUser` varchar(5) NOT NULL,
  `temp` varchar(5) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `x` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=148 ;

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `typeOfOperation` varchar(20) NOT NULL,
  `numberOfOperation` varchar(20) NOT NULL,
  `maxNumber` varchar(20) NOT NULL,
  `thrueDigit` varchar(20) NOT NULL,
  `quantityNumber` varchar(20) NOT NULL,
  `variable` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
